#include "app.h"
#include "circle.h"
#include "line.h"
#include "polygon.h"
#include "triangle.h"
#include "vertex.h"

#include <math.h>

App::App() 
{
    m_isRunning = true;
    m_window = NULL;
    m_renderer = NULL;    
}

int App::onExecute() 
{
    SDL_Event event;

    if (onInit() == false)
        return -1;

    while (m_isRunning) 
    {
        while (SDL_PollEvent(&event) != 0)
            onEvent(&event);
        
        onLoop();
        onRender();
    }

    return 0;
}

bool App::onInit() 
{
    SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2

    // Create an application window with the following settings:
    m_window = SDL_CreateWindow(
        "2D primitives renderer",         // window title
        SDL_WINDOWPOS_CENTERED,           // initial x position
        SDL_WINDOWPOS_CENTERED,           // initial y position
        640,                              // width, in M_pixels
        480,                              // height, in M_pixels
        SDL_WINDOW_OPENGL                 // flags - see below
    );

    // Check that the window was successfully created
    if (m_window == NULL) {
        // In the case that the window could not be made...
        printf("Could not create window: %s\n", SDL_GetError());
        return false;
    }

    m_renderer = SDL_CreateRenderer(m_window, -1, 0);

    SDL_SetRenderDrawColor(m_renderer, 10, 10, 30, 255);
    SDL_RenderClear(m_renderer);

    m_rectangle = Rectangle(120, 120, 100, 100);
    m_rectangle.setColor(54, 98, 184, 255);
    m_rectangle.render(m_renderer);

    SDL_RenderPresent(m_renderer);
    return true;
}

void App::onEvent(SDL_Event *event) 
{
    if (event->type == SDL_QUIT)
        m_isRunning = false;
}

void App::onLoop() 
{
    SDL_SetRenderDrawColor(m_renderer, 10, 10, 30, 255);
    SDL_RenderClear(m_renderer);

    // Update rectangle position to animate it
    SDL_Rect bounds = m_rectangle.getBounds();
    if (bounds.x + bounds.w + m_rectangle_horiz_dir > 640)
        m_rectangle_horiz_dir = -1;
    else if (bounds.x + m_rectangle_horiz_dir < 0)
        m_rectangle_horiz_dir = +1;
    if (bounds.y + bounds.h + m_rectangle_vert_dir > 480)
        m_rectangle_vert_dir = -1;
    else if (bounds.y + m_rectangle_vert_dir < 0)
        m_rectangle_vert_dir = +1;
    
    m_rectangle.setBounds(bounds.x + m_rectangle_horiz_dir, bounds.y + m_rectangle_vert_dir, bounds.w, bounds.h);

    // Draw triangle
    Vertex a = { 80, 400 }, b = { 540, 400 }, c = { 320, 80 };
    Triangle triangle(a, b, c);
    triangle.setColor(20, 20, 60, 255);
    triangle.render(m_renderer);

    // Compute triangle's inscribed circle parameters
    float ab = std::sqrt(std::pow((b.x - a.x), 2) + std::pow((b.y - a.y), 2));
    float bc = std::sqrt(std::pow((c.x - b.x), 2) + std::pow((c.y - b.y), 2));
    float ca = std::sqrt(std::pow((a.x - c.x), 2) + std::pow((a.y - c.y), 2));
    float p = ab + bc + ca;
    float x = (a.x * bc + b.x * ca + c.x * ab) / p;
    float y = (a.y * bc + b.y * ca + c.y * ab) / p;

    float s = (ab + bc + ca) / 2;
    float ar = std::sqrt(s * (s-ab) * (s - bc) * (s - ca));
    float r = ar / s;

    // Draw circle
    Circle circle(x, y, r);
    circle.setColor(30, 30, 90, 255);
    circle.render(m_renderer);
    
    // Draw polygon (4 branches star)
    std::vector<Vertex> points;

    Vertex v1 = {
        int(x + r * std::cos(M_PI / 4)),
        int(y + r * std::sin(M_PI / 4))
    };
    Vertex v3 = {
        int(x + r * std::cos(M_PI * 3 / 4)),
        int(y + r * std::sin(M_PI * 3 / 4))
    };
    Vertex v5 = {
        int(x + r * std::cos(M_PI * 5 / 4)),
        int(y + r * std::sin(M_PI * 5 / 4))
    };
    Vertex v7 = {
        int(x + r * std::cos(M_PI * 7 / 4)),
        int(y + r * std::sin(M_PI * 7 / 4))
    };

    Vertex v2 = {
        v1.x - (v1.x - v3.x) / 2,
        v5.y + (v3.y - v5.y) / 2 + 20
    };

    Vertex v4 = {
        v3.x + (v1.x - v3.x) / 2 - 20,
        v5.y + (v3.y - v5.y) / 2
    };

    Vertex v6 = {
        v3.x + (v1.x - v3.x) / 2,
        v5.y + (v3.y - v5.y) / 2 - 20
    };

    Vertex v8 = {
        v1.x - (v1.x - v3.x) / 2 + 20,
        v5.y + (v3.y - v5.y) / 2
    };

    points.push_back(v1);
    points.push_back(v2);
    points.push_back(v3);
    points.push_back(v4);
    points.push_back(v5);
    points.push_back(v6);
    points.push_back(v7);
    points.push_back(v8);

    Polygon polygon(points);
    polygon.setColor(50, 50, 110, 255);
    polygon.render(m_renderer);

    // Draw background lines
    Line line1(0, 200, 640, 200);
    line1.setColor(255, 0, 0, 255);
    line1.render(m_renderer);

    Line line2(0, 280, 640, 280);
    line2.setColor(255, 0, 0, 255);
    line2.render(m_renderer);

    // Draw rectangle
    m_rectangle.render(m_renderer);

    // Draw foreground line
    Line line3(0, 240, 640, 240);
    line3.render(m_renderer);

    SDL_RenderPresent(m_renderer);
}

void App::onRender() 
{
    SDL_Delay(10);
}

void App::onExit() 
{
    SDL_DestroyWindow(m_window);
    m_window = NULL;
    // Clean up
    SDL_Quit();
}
