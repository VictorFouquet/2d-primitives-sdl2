#pragma once

#include <SDL2/SDL.h>
#include <array>

#include "rectangle.h"

class App
{
    public:
        App();

        int onExecute();
        bool onInit();
        void onEvent(SDL_Event *event);
        void onLoop();
        void onRender();
        void onExit();

    private:
        bool m_isRunning;
        SDL_Window *m_window;
        SDL_Renderer *m_renderer;

        Rectangle m_rectangle;
        int m_rectangle_horiz_dir = 1;
        int m_rectangle_vert_dir = 1;
        
        std::array<int, 3> m_redList { 51, 255, 255 };
        std::array<int, 3> m_greenList { 128, 128, 191 };
        std::array<int, 3> m_blueList { 204, 0, 0 };
};
