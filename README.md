# 2D Primitives Renderer

C++ 2D primitives renderer running with SDL2

## Prerequesites

libsdl2 : Install with `sudo apt-get install libsdl2-dev`.


## Build and run

Change `buildAndRun.sh` script's rights

```bash
sudo chmod +x buildAndRun.sh
```

Execute the script to build and run the program

```bash
./buildAndRun.sh
```
