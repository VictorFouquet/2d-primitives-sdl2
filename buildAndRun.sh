#!/bin/bash
CURRENT_DIR=$(pwd)
if [ ! -d "${CURRENT_DIR}/build" ]
then
    mkdir build
fi
cd build
cmake ..
make
./2dPrimitivesRenderer
